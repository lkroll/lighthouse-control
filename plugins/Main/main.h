/*
 * Copyright (C) 2023  Lennart Kroll
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * lighthouse-control is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef Main_H
#define Main_H

#include <QObject>
#include <QString>
#include <simpleble/SimpleBLE.h>

class Main: public QObject {
    Q_OBJECT

public:
    Q_INVOKABLE int testScan();
    Q_INVOKABLE int startScan();
    Q_INVOKABLE int startToggleLH(int index, const QString& status);
    Q_INVOKABLE int toggleLH(int index, const QString& status);
    int get_byte_array(const SimpleBLE::ByteArray& bytes);

signals:
    void bluetoothDisabled();
    void noAdapter();
    void readingCharacteristics();
    void lighthouseFound(const QString& name, const QString& address, const QString& status);
    void scanStarted();
    void scanFinished(int result);
    void toggleFinished(int index, const QString& status);
};

#endif
