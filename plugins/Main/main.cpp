/*
 * Copyright (C) 2023  Lennart Kroll
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * lighthouse-control is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QDebug>
#include <QString>
#include <iostream>
#include <iomanip>
#include <thread>

#include <simpleble/SimpleBLE.h>

#include "main.h"

std::vector<SimpleBLE::Peripheral> lighthouses;
SimpleBLE::BluetoothUUID service_uuid = "00001523-1212-efde-1523-785feabcd124";
SimpleBLE::BluetoothUUID characteristic_uuid = "00001525-1212-efde-1523-785feabcd124";

int Main::get_byte_array(const SimpleBLE::ByteArray& bytes) {
    return ((uint32_t)((uint8_t)bytes.front()));
}

// Test for Bluetooth capabilities
int Main::testScan() {
    auto adapters = SimpleBLE::Adapter::get_adapters();

    if (adapters.empty()) {
        std::cout << "No Bluetooth adapters found" << std::endl;
        emit noAdapter();
        return 1;
    }

    if (!SimpleBLE::Adapter::bluetooth_enabled()) {
        std::cout << "Bluetooth is not enabled" << std::endl;
        emit bluetoothDisabled();
        return 1;
    }

    std::thread worker(&Main::startScan, this);
    worker.detach();
    return 0;
}

// Start Bluetooth Scan
int Main::startScan() {

    emit scanStarted();
    auto adapters = SimpleBLE::Adapter::get_adapters();

    // Use the first adapter
    auto adapter = adapters[0];

    // Bluetooth adapter info
    std::cout << "Adapter identifier: " << adapter.identifier() << std::endl;
    std::cout << "Adapter address: " << adapter.address() << std::endl;

    std::cout << "Scanning for BLE devices..." << std::endl;
    adapter.scan_for(2500);

    // Get the list of peripherals found
    std::vector<SimpleBLE::Peripheral> peripherals = adapter.scan_get_results();

    for (auto peripheral : peripherals) {
        if (peripheral.identifier().find("LHB") != std::string::npos) {
            lighthouses.push_back(peripheral);
        }
    }

    if (lighthouses.empty()) {
        emit scanFinished(1);
        std::cout << "No lighthouses found" << std::endl;
        return 1;
    }

    emit readingCharacteristics();

    for (auto peripheral : lighthouses) {
        std::cout << "Found lighthouse " << peripheral.identifier() << ", Getting current state..." << std::endl;

        peripheral.connect();

        SimpleBLE::ByteArray rx_data = peripheral.read(service_uuid, characteristic_uuid);

        // Base station is on
        if (get_byte_array(rx_data) == 11) {
            std::cout << "Current state: ON" << std::endl;
            emit lighthouseFound(QString::fromStdString(peripheral.identifier()),QString::fromStdString(peripheral.address()),QString::fromStdString("ON"));
        }

        // Base station is off
        if (get_byte_array(rx_data) == 0) {
            std::cout << "Current state: OFF" << std::endl;
            emit lighthouseFound(QString::fromStdString(peripheral.identifier()),QString::fromStdString(peripheral.address()),QString::fromStdString("OFF"));
        }

        peripheral.disconnect();
    }

    emit scanFinished(0);
    return 0;
}

// Start toggleLH in new thread to avoid blocking QML thread
int Main::startToggleLH(int index, const QString& status) {
    std::thread worker(&Main::toggleLH, this, index, status);
    worker.detach();
    return 0;
}

// Toggle lighthouse
int Main::toggleLH(int index, const QString& status) {
    auto adapters = SimpleBLE::Adapter::get_adapters();

    // Use the first adapter
    auto adapter = adapters[0];

    SimpleBLE::Peripheral peripheral = lighthouses[index];

    peripheral.connect();

    // if on turn off
    if (status.toStdString() == "ON") {
        std::cout << "Turning off " << peripheral.identifier() << std::endl;
        peripheral.write_request(service_uuid, characteristic_uuid, "0");
        peripheral.disconnect();
        emit toggleFinished(index,QString::fromStdString("OFF"));
    }

    // if off turn on
    if (status.toStdString() == "OFF") {
        std::cout << "Turning on " << peripheral.identifier() << std::endl;
        peripheral.write_request(service_uuid, characteristic_uuid, "1");
        peripheral.disconnect();
        emit toggleFinished(index,QString::fromStdString("ON"));
    }

    return 0;
}
