/*
 * Copyright (C) 2023  Lennart Kroll
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * lighthouse-control is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QtQml>
#include <QtQml/QQmlContext>

#include "plugin.h"
#include "main.h"

void Plugin::registerTypes(const char *uri) {
    //@uri Main
    qmlRegisterSingletonType<Main>(uri, 1, 0, "Main", [](QQmlEngine*, QJSEngine*) -> QObject* { return new Main; });
}
