/*
 * Copyright (C) 2023  Lennart Kroll
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * lighthouse-control is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import QtQuick.Controls 2.2
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0

import Main 1.0

MainView {

    id: root
    objectName: 'mainView'
    applicationName: 'lighthouse-control.lkroll'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

    ListModel {
        id: lighthouseListModel
    }

    Page {
        anchors.fill: parent

        header: PageHeader {
            id: header
            title: i18n.tr('Lighthouse Control')

            trailingActionBar {
                actions: [
                    Action {
                      iconName: 'info'
                      onTriggered: PopupUtils.open(infoDialogPopup)
                    }
                ]
            }
        }

        Rectangle {
            id: startScanRectangle
            height: units.gu(4)
            color: theme.name == "Lomiri.Components.Themes.Ambiance" ? LomiriColors.silk : LomiriColors.slate
            anchors {
                top: header.bottom
                left: parent.left
                right: parent.right
            }
            Label {
                id: startScanLabel
                text: i18n.tr('Tap here to start lighthouse scan')
                textSize: Label.Large
                anchors.centerIn: parent
            }
            MouseArea {
                id: startScanMouseArea
                anchors.fill: parent
                onClicked: {
                    Main.testScan();
                }
            }
        }

        ProgressBar {
          id: progressBar
          indeterminate: true
          visible: false
          anchors {
              top: startScanRectangle.bottom
              right: parent.right
              left: parent.left
          }
        }

        Connections {
            target: Main
            onScanFinished: {
                if (result == 0) {
                    startScanLabel.text = i18n.tr('Scan completed');
                    progressBar.visible = false;
                }
                if (result == 1) {
                    startScanLabel.text = i18n.tr('No lighthouses found, scan again?');
                    progressBar.visible = false;
                    startScanMouseArea.enabled = true;
                }
            }
            onScanStarted: {
                startScanLabel.text = i18n.tr('Scanning for lighthouses...');
                progressBar.visible = true;
                startScanMouseArea.enabled = false;
                placeholderIcon.visible = false;
                placeholderLabel.visible = false;
            }
            onReadingCharacteristics: {
                startScanLabel.text = i18n.tr('Lighthouses found, gathering info...');
            }
        }

        Image {
            id: placeholderIcon
            source: '../assets/arrow-up-circle.svg'
            fillMode: Image.PreserveAspectFit
            width: units.gu(8)
            height: units.gu(8)
            anchors {
                centerIn: parent
                verticalCenterOffset: units.gu(-4)
            }
        }

        Label {
            id: placeholderLabel
            text: i18n.tr('Nothing here, tap above to start looking for lighthouses');
            textSize: Label.Large
            verticalAlignment: Label.AlignVCenter
            horizontalAlignment: Label.AlignHCenter
            wrapMode: Label.Wrap
            anchors {
                top: startScanRectangle.bottom
                bottom: parent.bottom
                right: parent.right
                rightMargin: units.gu(4)
                left: parent.left
                leftMargin: units.gu(4)
            }
        }

        ListView {
            id: lighthouseListView
            anchors {
                top: startScanRectangle.bottom
                bottom: parent.bottom
                left: parent.left
                right: parent.right
            }
            model: lighthouseListModel

            delegate: ListItem {
                width: parent.width
                height: units.gu(15)
                Column {
                    spacing: units.gu(1)
                    anchors {
                        left: parent.left
                        leftMargin: units.gu(2)
                        top: parent.top
                        topMargin: units.gu(2.5)
                        bottom: parent.bottom
                    }
                    Label {
                        id: lighthouseState
                        text: status
                        textSize: Label.Medium
                        color: text == "ON" ? LomiriColors.green : LomiriColors.red
                    }
                    Label {
                        id: lighthouseName
                        text: name
                        textSize: Label.Large
                    }
                    Label {
                        id: lighthouseAdress
                        text: address
                        textSize: Label.Medium
                    }
                }
                Image {
                    source: '../assets/power-button.svg'
                    id: powerButton
                    fillMode: Image.PreserveAspectFit
                    height: units.gu(8)
                    width: units.gu(8)
                    anchors {
                        right: parent.right
                        rightMargin: units.gu(2)
                        verticalCenter: parent.verticalCenter
                    }
                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            powerButton.visible = false;
                            loadingIndicator.running = true;
                            Main.startToggleLH(index, lighthouseState.text);
                        }
                    }
                }
                ActivityIndicator {
                    id: loadingIndicator
                    height: units.gu(5)
                    width: units.gu(5)
                    anchors {
                        right: parent.right
                        rightMargin: units.gu(4)
                        verticalCenter: parent.verticalCenter
                    }
                }
                Connections {
                    target: Main
                    onToggleFinished: {
                        loadingIndicator.running = false;
                        powerButton.visible = true;
                        lighthouseListModel.setProperty(index, "status", status);
                    }
                }
            }
        }

        Connections {
            target: Main
            onLighthouseFound: {
                lighthouseListModel.append({
                    "name": name,
                    "address": address,
                    "status": status
                });
            }
        }
    }

    // info popup
    Component {
        id: infoDialogPopup
        Dialog {
            id: infoDialog
            title: i18n.tr('About\n')
            Image {
                source: '../assets/logo.png'
                height: units.gu(15)
                fillMode: Image.PreserveAspectFit
            }
            Label {
                text: i18n.tr('\nA simple app to start and stop your VR lighthouses from your UT device.')
                horizontalAlignment: Label.AlignHCenter
                wrapMode: Label.Wrap
            }
            Label {
                text: i18n.tr('Sourcecode: ') + "<a href='https://gitlab.com/lkroll/lighthouse-control'>Gitlab</a>"
                horizontalAlignment: Label.AlignHCenter
                wrapMode: Label.Wrap
                onLinkActivated: Qt.openUrlExternally(link)
            }
            Button {
                color: theme.name == "Lomiri.Components.Themes.Ambiance" ? LomiriColors.silk : LomiriColors.slate
                text: i18n.tr('Close')
                onClicked: PopupUtils.close(infoDialog)
            }
        }
    }

    Component {
        id: bluetoothDisabledDialogPopup
        Dialog {
            id: bluetoothDisabledDialog
            title: i18n.tr('Bluetooth is not enabled')
            text: i18n.tr('Please enable it in the system settings and try again')
            Button {
                text: i18n.tr('Close')
                color: LomiriColors.red
                onClicked: PopupUtils.close(bluetoothDisabledDialog)
            }
        }
    }

    Component {
        id: noAdapterDialogPopup
        Dialog {
            id: noAdapterDialog
            title: i18n.tr('No Bluetooth adapter found')
            Button {
                id: closeButton
                text: i18n.tr('Close')
                onClicked: PopupUtils.close(noAdapterDialog)
            }
        }
    }

    Connections {
        target: Main
        onBluetoothDisabled: {
            PopupUtils.open(bluetoothDisabledDialogPopup);
        }
        onNoAdapter: {
            PopupUtils.open(noAdapterDialogPopup);
        }
    }

}
