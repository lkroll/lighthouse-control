��          �            h  H   i     �     �     �     �  $   �       !   +  8   M  5   �     �     �     �  !   �  k    N   �     �     �     �     �  1     (   @  ?   i  D   �  +   �       &   1  
   X  )   c        	                                                        
                
A simple app to start and stop your VR lighthouses from your UT device. About
 Bluetooth is not enabled Close Lighthouse Control Lighthouses found, gathering info... No Bluetooth adapter found No lighthouses found, scan again? Nothing here, tap above to start looking for lighthouses Please enable it in the system settings and try again Scan completed Scanning for lighthouses... Sourcecode:  Tap here to start lighthouse scan Project-Id-Version: lighthouse-control.lkroll
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-07-08 14:55+0200
Last-Translator: Heimen Stoffels <vistausss@fastmail.com>
Language-Team: Dutch <>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.4.2
 
Een eenvoudige app voor het bedienen van VR-lighthouses vanaf je UT-apparaat. Over
 Bluetooth is uitgeschakeld Sluiten Lighthousebediening Bezig met opvragen van basisstationsinformatie… Er is geen bluetoothadapter aangetroffen Er zijn geen basisstations aangetroffen. Wil je opnieuw zoeken? Er zijn geen basisstations beschikbaar. Druk hierboven om te zoeken. Schakel bluetooth in en probeer het opnieuw Het zoeken is afgerond Bezig met zoeken naar basisstations… Broncode:  Druk hier om te zoeken naar basisstations 