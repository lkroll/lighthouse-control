��          �            h  H   i     �     �     �     �  $   �       !   +  8   M  5   �     �     �     �  !   �      L   *     w  !   ~  
   �     �  (   �     �        =   (  G   f     �     �     �  !   �        	                                                        
                
A simple app to start and stop your VR lighthouses from your UT device. About
 Bluetooth is not enabled Close Lighthouse Control Lighthouses found, gathering info... No Bluetooth adapter found No lighthouses found, scan again? Nothing here, tap above to start looking for lighthouses Please enable it in the system settings and try again Scan completed Scanning for lighthouses... Sourcecode:  Tap here to start lighthouse scan Project-Id-Version: lighthouse-control.lkroll
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-07-08 18:25+0200
Last-Translator: 
Language-Team: 
Language: de_DE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 3.3.2
 
Eine simple App um deine VR Basisstationen von deinem UT Gerät zu steuern. Über
 Bluetooth ist nicht eingeschaltet Schließen Lighthouse Control Basisstationen gefunden, hole Details... Kein Bluetooth Adapter gefunden Nichts gefunden, erneut scannen? Noch nichts hier, tippe oben um nach Basisstationen zu suchen Bitte schalte es in den Systemeinstellungen ein und versuche es nochmal Scan abgeschlossen Suche nach Basisstationen... Quelltext:  Tippe hier um Suchlauf zu starten 