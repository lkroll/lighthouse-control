# Lighthouse Control

Control your Valve Index lighthouses / base stations from your UT device

[![OpenStore](https://open-store.io/badges/en_US.png)](https://open-store.io/app/lighthouse-control.lkroll)

## Build instructions

Make sure to clone the repository including its submodules like so:

```
git clone --recurse-submodules https://gitlab.com/lkroll/lighthouse-control.git
```

First build the libraries for your specific architecture (arm64 as an example).
Then build the app with clickable build and accept review errors, because bluetooth policy is reserved!
Afterwards just install via clickable install.

```
clickable build --libs --arch arm64
clickable build --arch arm64 --accept-review-errors
clickable install --arch arm64
```

## License

Copyright (C) 2023  Lennart Kroll

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
